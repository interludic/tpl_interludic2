<?php
/**
* @version 			SEBLOD 3.x More ~ $Id: index.php sebastienheraud $
* @package			SEBLOD (App Builder & CCK) // SEBLOD nano (Form Builder)
* @url				http://www.interludic.com.au
* @editor			Interludic - www.interludic.com.au
* @copyright		Copyright (C) 2016 INTERLUDIC. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
**/
defined( '_JEXEC' ) or die;

// -- Initialize
require_once dirname(__FILE__).'/config.php';
$cck	=	CCK_Rendering::getInstance( $this->template );

if ( $cck->initialize() === false ) { return; }

// -- Prepare
$attributes		=	$cck->item_attributes ? ' '.$cck->item_attributes : '';
$class			=	trim( $cck->getStyleParam( 'class', '' ) );
$custom_attr	=	trim( $cck->getStyleParam( 'attributes', '' ) );
$custom_attr	=	$custom_attr ? ' '.$custom_attr : '';

$html			=	'';
$id_class		=	$cck->id_class;
$items			=	$cck->getItems();
$count			=	count( $items );

// $fieldnames		=	$cck->getFields( 'element', '', false );
$multiple		=	( count( $fieldnames ) > 1 ) ? true : false;

		// echo "<pre>";
		// var_dump($cck);
		// echo "</pre><hr />";	
// -- Render
$html .= "<div class='flexslider'>";
$html .= "<ul class='slides'>";

if ( $count ) {
	foreach ( $items as $item ) {
		$row	=	'';

		$galleryCaption = $item->getValue('hero_gallery_caption');
		$galleryVideo = $item->getValue('hero_gallery_video');
		
		if(!empty($galleryVideo)){
			$media = "<iframe id='player_1' width='500' height='281' src='".$galleryVideo."' frameborder='0' allowfullscreen></iframe>";
			// $media = "<iframe width='500' height='281' src='https://www.youtube.com/watch?v=FPIU8KlBcug' frameborder='0' allowfullscreen></iframe>";
			// $media = "<iframe id='player_1' src='http://player.vimeo.com/video/56287630?api=1' width='500' height='281' allowFullScreen></iframe>";
			$galleryVideoCheck = 1;
		} else {
			$media = "<img alt='".$galleryCaption."' src='".$item->getValue('hero_gallery_image')."' />";			
		}

		$row	.=	"<li>".$media."";
		$row	.=  "<p class='flex-caption'>".$galleryCaption."</p></li>";
		$html	.=	$row;
	}
	$html	.=	"</ul>";
	$html	.=	"</div>";

	echo $html;
}

$cck->finalize(); // -- Finalize 
?>