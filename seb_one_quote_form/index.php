<?php
/**
* @version 			SEBLOD 3.x Core ~ $Id: index.php alexandrelapoux $
* @package			SEBLOD (App Builder & CCK) // SEBLOD nano (Form Builder)
* @url				http://www.seblod.com
* @editor			Octopoos - www.octopoos.com
* @copyright		Copyright (C) 2013 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
**/

defined( '_JEXEC' ) or die;

// -- Initialize
require_once dirname(__FILE__).'/config.php';
$cck	=	CCK_Rendering::getInstance( $this->template );
if ( $cck->initialize() === false ) { return; }

// -- Prepare
// $attributes =   $cck->id_attributes ? ' '.$cck->id_attributes : '';
// $attributes =   $cck->replaceLive( $attributes );

// if ( $nLeft && $nRight ) {
// 	$css	=	'#'.$cck->id.'_m100.cck-m100 {margin: 0 '.$cck->getStyleParam( 'position_right' ).'px 0 '.$cck->getStyleParam( 'position_left' ).'px !important;}'."\n";
// 	$css	.=	'#'.$cck->id.' .cck-line-left {width: '.$cck->getStyleParam( 'position_left' ).'px;}'."\n";
// 	$css	.=	'#'.$cck->id.' .cck-line-right {width: '.$cck->getStyleParam( 'position_right' ).'px; margin-left: -'.( $cck->getStyleParam( 'position_left' ) + $cck->getStyleParam( 'position_right' ) ).'px;}';
// } elseif ( $nLeft ) {
// 	$css 	=	'#'.$cck->id.'_m100.cck-m100 {margin: 0 0 0 '.$cck->getStyleParam( 'position_left' ).'px !important;}'."\n";
// 	$css	.=	'#'.$cck->id.' .cck-line-left {width: '.$cck->getStyleParam( 'position_left' ).'px;}';
// } elseif ( $nRight ) {
// 	$css	=	'#'.$cck->id.'_m100.cck-m100 {margin: 0 '.$cck->getStyleParam( 'position_right' ).'px 0 0 !important;}'."\n";
// 	$css	.=	'#'.$cck->id.' .cck-line-right {width: '.$cck->getStyleParam( 'position_right' ).'px; margin-left: -'.( $cck->getStyleParam( 'position_right' ) ).'px;}';
// } else {
// 	$css	=	'#'.$cck->id.'_m100.cck-m100 {margin: 0 0 0 '.$cck->getStyleParam( 'position_left' ).'px !important;}' ;
// }
$css = "label { display: block; font-weight:600; font-size:16px;}";
$css .= "fieldset.checkboxes input { float: left; }";
$css .= "fieldset.checkboxes label { font-weight:initial; font-size:12px; }";
$cck->addStyleDeclaration( $css );

// -- Render
// echo "<pre>";
// var_dump($cck);
// echo "</pre>";
?>
<?php //echo $cck->renderField('qf_email_reception'); ?>

<!-- HEADER -->
<div id="<?php //echo $cck->id; ?>" class="container page header <?php //echo $cck->id_class; ?>" <?php //echo $attributes; ?>>
		<?php // header-line
		if ( $cck->getStyleParam( 'position_header' ) ) {
			if ( $n = $cck->countFields( 'header', true ) ) {
				//$catTitle = $cck->renderPositions( 'header', $cck->getStyleParam( 'position_header_variation', '' ), $n );
			}
		} ?>
		
		<?php // top-line
		if ( $cck->getStyleParam( 'position_top' ) ) {
			if ( $n = $cck->countFields( 'top', true ) ) {
				//echo $cck->renderPositions( 'top', $cck->getStyleParam( 'position_top_variation', '' ), $n, '' );
			}
		} ?>
	
	<?php if ( $cck->countFields( 'topbody' ) ) { 
		$title = $cck->renderPosition( 'topbody', '', '' ); 
	} ?>
	   
	<h1 class="title">Quote Request<?php //echo $title; ?></h1>
	<p class="lead">Enter your project details below<?php //echo $catDesc; ?></p>
</div>

<!-- BODY -->
<div class="container page body"> 

	<?php if ( $cck->countFields( 'mainbody' ) ) { 
		echo $cck->renderPosition( 'mainbody', '', '' ); 
	} ?>

	<?php // hidden
	if ( $cck->countFields( 'modal' ) && JCck::on() ) {
		JHtml::_( 'bootstrap.modal', 'collapseModal' );
		?>
		<div class="modal hide fade" id="collapseModal">
			<?php echo $cck->renderPosition( 'modal' ); ?>
		</div>
	<?php } ?>

	<?php // hidden
	if ( $cck->countFields( 'hidden' ) ) { ?>
		<div style="display: none;">
			<?php echo $cck->renderPosition( 'hidden' ); ?>
		</div>
	<?php } ?>

	<?php // debug
	if ( $cck->doDebug() ) { ?>
		<div class="cck-line-debug">	
			<div class="cck-w100 cck-fl cck-ptb">
				<div class="cck-plr"><?php echo $cck->renderPosition( 'debug', 'none' ); ?></div>
			</div>
		</div>            
	<?php } ?>

</div><!-- ./container page body -->

<?php
// -- Finalize
$cck->finalize();
?>