<?php
/**
* @version 			SEBLOD 3.x More ~ $Id: index.php sebastienheraud $
* @package			SEBLOD (App Builder & CCK) // SEBLOD nano (Form Builder)
* @url				http://www.seblod.com
* @editor			Octopoos - www.octopoos.com
* @copyright		Copyright (C) 2013 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
**/
defined( '_JEXEC' ) or die;

// -- Initialize
require_once dirname(__FILE__).'/config.php';
$cck	=	CCK_Rendering::getInstance( $this->template );
if ( $cck->initialize() === false ) { return; }

// -- Prepare
$attributes		=	$cck->item_attributes ? ' '.$cck->item_attributes : '';
$class			=	trim( $cck->getStyleParam( 'class', '' ) );
$custom_attr	=	trim( $cck->getStyleParam( 'attributes', '' ) );
$custom_attr	=	$custom_attr ? ' '.$custom_attr : '';
$html			=	'';
$id_class		=	$cck->id_class;
$items			=	$cck->getItems();
$fieldnames		=	$cck->getFields( 'element', '', false );
$multiple		=	( count( $fieldnames ) > 1 ) ? true : false;
$count			=	count( $items );
$isRaw			=	( $count == 1 ) ? $cck->getStyleParam( 'auto_clean', 0 ) : 0;

// Set
$isMore			=	$cck->isLoadingMore();
if ( $cck->isGoingToLoadMore() ) {
	$class		=	trim( $class.' '.'cck-loading-more' );
}
$class			=	str_replace( '$total', $count, $class );
$class			=	$class ? ' class="'.$class.'"' : '';

// -- Render
if ( !( $isRaw || $isMore ) ) { 
	////////////////////////////////
	//Get content for page header
	foreach ( $items as $item ) {		
		$catIdArt = $item->get('art_catid');
		
		if(!empty($catIdArt->value)){
			$catTitle = $catIdArt->text;
			$catDesc = getCatDesc($catIdArt->value);
	    $catDesc = CCK_Content::getValue( $catDesc->description, 'description' );
			// echo "<pre>";
			// var_dump($catIdArt->value);
			// var_dump($catTitle);
			// var_dump($catDesc);
			// echo "</pre>";
			break;
		}		
	}
?>

<!-- HEADER -->
<div class="container page header">
    <h1 class="title"><?php echo $catTitle; ?></h1>
    <p class="lead"><?php echo $catDesc; ?></p>
</div>

<!-- BODY -->
<div class="container page body <?php echo $class.$custom_attr; ?>"> 

<?php }
	if ( $count ) {
		
		///////////////////////////////////
		//Stories
		// echo count($fieldnames)." attrib=".$item->replaceLive( $attributes )." ".$attributes."<pre>";
		// var_dump($fieldnames);
		// echo "</pre>";
		foreach ( $items as $item ) {
			$cta = $item->get('ss_cta');
			$contactId = $item->getValue('ss_contact');			
			
			// echo "<pre>";
			// 	// // // renderField
			// 	// $id = $item->renderField('ss_problem_desc');
			// 	var_dump($item);
			// echo "</pre>";

			$row	=	"
			<!-- ITEM ".$item->id." -->
		  <div class='row story'>
		    <div class='col-md-1'>
		      <div class='item-thumb-left story hidden-sm-down'>
		        <img src='".$item->getValue( 'ss_solution_image' )."' class='img-fluid center-block' />
		      </div>
		      <div class='story hidden-md-up'>
		        <img src='".$item->getValue( 'ss_solution_image' )."' class='img-fluid center-block' />
		      </div>
		    </div>

		    <div class='col-md-4'>
		      <h2>".$item->getValue( 'ss_solution_title' )."</h2>

		      <p class='attrib'>Completed date: <span class='value'>".$item->getText( 'art_created' )."</span></p>
		      <p>".$item->getValue( 'ss_solution_description' )."</p>";

		    if(!empty($cta->link)){
		    	$row .=	"
		      <div class='center-block-small ss_cta'>
		        <a class='btn btn-lg btn-primary-light cbutton cbutton--effect-marko' href='".$cta->link."' role='button'>".($item->getValue('ss_cta_text') ?: $cta->text)."</a>
		      </div>";
		    }
		    
		    $row .=	"
		      <hr class='hidden-md-up'/>
		    </div>
		    
		    <div class='col-md-7'>
		     ";

		    ///////////
		    //CONTACT
				if($contactId){
					$contact = getContact($contactId);
			    $row .=	"
		      	<div class='row'>
			        <div class='col-sm-8 col-xs-6'>
			          <p>Role: ".$contact->con_position."</p>                  
			          <h2 class='brand'>".$contact->name."</h2>
			          <p><a class='btn btn-sm btn-primary-light cbutton cbutton--effect-marko' href='mailto:".$contact->email_to."?Subject=Barra%20Steel' role='button' target='_top'>Contact</a></p>
			          <p>".$item->getValue( 'ss_contact_task' )."</p>
			        </div>

			        <div class='col-sm-4 col-xs-6'>
			          <img src='".$contact->image."' class='img-fluid center-block' alt='alt'/>
			        </div>
            </div>
			      
			      <hr />";
				} 
		    

		    ///////////////
		    //PROBLEM
		    $row .=	"
		      <div class='row'>
		        <div class='col-sm-6'>
		          <h3 class='problem brand'>".$item->getValue( 'ss_problem_title' )."</h3>
		          <p>".$item->getValue( 'ss_problem_desc' )."</p>		       
		        </div>";
		    
		    if($item->getValue( 'ss_problem_image' )){
			    $row .=	"
		        <div class='col-sm-6'>
		          <img src='".$item->getValue( 'ss_problem_image' )."' class='img-fluid center-block' alt='".$item->getValue( 'ss_problem_title' )."' />                
		        </div>";
		    }	

		    $row .=	"		    
		      </div>
		    </div>		    
		  </div><!-- ./success story -->";

			//Fields
			// foreach ( $fieldnames as $fieldname ) {

			// 	$content	=	$item->renderField( $fieldname );
			// 	if ( $content != '' ) {
			// 		echo $fieldname." ".$content;

			// 		if ( $item->getMarkup( $fieldname ) != 'none' && ( $multiple || $item->getMarkup_Class( $fieldname ) ) ) {
			// 			$row	.=	'<div class="cck-clrfix'.$item->getMarkup_Class( $fieldname ).'">Field : '.$content.'</div>';
			// 		} else {
			// 			$row	.=	$content;
			// 		}
			// 	}
			// }
    
      $row .= "<div class='row item-seperator-padding'>
                  <div class='col-xs-12 item-seperator'>
                  </div>
                </div> <!-- ./item seperator -->";

      $html	.=	$row;		
		}		
		echo $html;
	}


if ( !( $isRaw || $isMore ) ) { ?>

</div><!-- ./container page body -->


<?php 
}
// -- Finalize
$cck->finalize();

function getContact($id){
	// Get a db connection.
	$db = JFactory::getDbo();
	 
	// Create a new query object.
	$query = $db->getQuery(true);
	 
	// Select all records from the user profile table where key begins with "custom.".
	// Order it by the ordering field.
	$query->select($db->quoteName(array('name', 'con_position', 'email_to', 'image')));
	$query->from($db->quoteName('#__contact_details'));
	$query->where($db->quoteName('id') . ' = '. $id);//$db->quote('\'custom.%\'')
	 
	// Reset the query using our newly populated query object.
	$db->setQuery($query);
	 
	// Load the results as a list of stdClass objects (see later for more options on retrieving data).
	return $db->loadObject();
}

function getCatDesc($id){
	// Get a db connection.
	$db = JFactory::getDbo();
	 
	// Create a new query object.
	$query = $db->getQuery(true);
	 
	// Select all records from the user profile table where key begins with "custom.".
	// Order it by the ordering field.
	$query->select($db->quoteName(array('description', 'params')));
	$query->from($db->quoteName('#__categories'));
	$query->where($db->quoteName('id') . ' = '. $id);//$db->quote('\'custom.%\'')
	 
	// Reset the query using our newly populated query object.
	$db->setQuery($query);

	// Load the results as a list of stdClass objects (see later for more options on retrieving data).
	return $db->loadObject();
}

?>