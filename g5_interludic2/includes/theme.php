<?php
/**
 * @package   Gantry5
 * @author    Interludic http://www.interludic.com.au
 * @copyright Copyright (C) 2007 - 2016 Interludic, LLC
 * @license   GNU/GPLv2 and later
 *
 * http://www.gnu.org/licenses/gpl-2.0.html
 */

class_exists('\\Gantry\\Framework\\Gantry') or die;

// Define the template.
class GantryTheme extends \Gantry\Framework\Theme {}

// Initialize theme stream.
$gantry['platform']->set(
    'streams.gantry-theme.prefixes',
    ['' => [
        "gantry-themes://{$gantry['theme.name']}/custom",
        "gantry-themes://{$gantry['theme.name']}",
        "gantry-themes://{$gantry['theme.name']}/common"
    ]]
);

// Define Gantry services.
$gantry['theme'] = function ($c)  {
    return new GantryTheme($c['theme.path'], $c['theme.name']);
};

// Require the custom php library
include_once dirname(__FILE__).'/../custom/includes/interludic2_helper.php';

// Dependency Injection of hero background images
$gantry['hero_background'] = new Hero_Background();

