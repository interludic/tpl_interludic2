<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.core');

?>

<?php if (empty($this->items)) : ?>
	<p> <?php echo JText::_('COM_CONTACT_NO_CONTACTS'); ?>	 </p>
<?php else : ?>

<?php 
	//GET header category details
	// $app = JFactory::getApplication();
	$db = JFactory::getDBO();
	
	foreach ($this->items as $i => $item) {		
		if(!empty($item->catid)){
			$db->setQuery("SELECT description, title FROM #__categories WHERE id = ".$item->catid." LIMIT 1;");
			$catDetails = $db->loadObject();
			break;
		}
	}
?>
	
<div class="container page header">
	<h1 class="title" itemprop="name"><?php echo $this->escape($catDetails->title); ?></h1>
	<?php echo CCK_Content::getValue( $catDetails->description, 'description' ); ?>
</div>

<!-- BODY -->
<div class="container page body">
	<form action="<?php echo htmlspecialchars(JUri::getInstance()->toString()); ?>" method="post" name="adminForm" id="adminForm">
			<?php foreach ($this->items as $i => $item) : ?>
				<?php if ($this->items[$i]->published == 1) : ?>
					<div class="row" style="min-height: 100px;">
					
						<?php 
							// echo "<pre>";
							// // var_dump($catDetails); 
							// var_dump($item); 
							// echo "</pre>";
						?>

						<div class="col-md-2">
							<?php if ($item->image) : ?>
								<div class="pull-left">
									<img src="<?php echo $item->image; ?>" alt="<?php echo $item->name; ?>'s Profile" style="width: 100px; height: 100px; margin-right: 10px;">
								</div>							
							<?php endif; ?>
						</div>
						<div class="col-md-4">
							<h3><?php echo $item->name; ?></h3>
							<!-- <a href="<?php echo JRoute::_(ContactHelperRoute::getContactRoute($item->slug, $item->catid)); ?>">pop?	</a> -->

							<?php echo $item->event->afterDisplayTitle; ?>
							<?php echo $item->event->beforeDisplayContent; ?>

							<?php if ($this->params->get('show_position_headings')) : ?>
								<?php echo $item->con_position; ?><br />
							<?php endif; ?>

							<?php if ($item->email_to) : ?>
								<h5><a href="mailto:<?php echo $item->email_to; ?>" target="_blank"><i class="fa fa-envelope"></i></a></h5>
							<?php endif; ?>

							<?php if ($this->params->get('show_suburb_headings') AND !empty($item->suburb)) : ?>
								<?php //echo $item->suburb . ', '; ?>
							<?php endif; ?>
							<?php if ($this->params->get('show_state_headings') AND !empty($item->state)) : ?>
								<?php //echo $item->state . ', '; ?>
							<?php endif; ?>
							<?php if ($this->params->get('show_country_headings') AND !empty($item->country)) : ?>
								<?php //echo $item->country; ?><br />
							<?php endif; ?>
							<?php if ($this->params->get('show_telephone_headings') AND !empty($item->telephone)) : ?>
								<?php //echo JText::sprintf('COM_CONTACT_TELEPHONE_NUMBER', $item->telephone); ?><br />
							<?php endif; ?>
							<?php if ($this->params->get('show_fax_headings') AND !empty($item->fax) ) : ?>
								<?php //echo JText::sprintf('COM_CONTACT_FAX_NUMBER', $item->fax); ?><br />
							<?php endif; ?>
						</div>
						
						<div class="col-md-6">
							<?php echo $item->misc; ?>
						</div>

					<?php echo $item->event->afterDisplayContent; ?>
					</div>
					<hr />
				<?php endif; //published item ?>
			<?php endforeach; ?>
	</form>
</div>
<?php endif; ?>