<?php
defined('_JEXEC') or die;
define('JPATH_BASE', '../../../../');
require_once ( JPATH_BASE .'/includes/defines.php' );
require_once ( JPATH_BASE .'/includes/framework.php' );

/**
* 	Grab the background image for the current page using the menu item / image
*/

class Hero_Background
{
	
	/**
	 * Get image from menu item
	 *
	 * @return void
	 * @author 
	 **/
	public function image()
	{

		$app = JFactory::getApplication();
		$currentMenuId = JSite::getMenu()->getActive()->id;
		$menuitem   = $app->getMenu()->getItem($currentMenuId);
		$params = $menuitem->params;
		if(empty($params['menu_image'])) return JURI::root()."/images/hero/bg/hero_bg.jpg";
			else return JURI::root()."/".$params['menu_image'];

		// TODO if we are not on a menu item, search through category image.
		// // $input = JFactory::getApplication()->input;
		// 		 $app = Jfactory::getApplication();
		//     $input=$app->input;
		//  if ($input->getCmd('option')=='com_content' 
		//     && $input->getCmd('view')=='article' ){
		//         $cmodel = JModelLegacy::getInstance('Article', 'ContentModel');
		//         $catid = $cmodel->getItem($app->input->get('id'))->catid;
		//     }

		// return $catid;
		// $secid = $this->item->catid;
		// return $secid;
		// $this->section = & JTable::getInstance( 'category' );
		// $this->section->load( $secid );
		// $cparams = JComponentHelper::getParams ('com_media');
		// return $cparams->get('image_path').'/'.$this->section->image;
		// return "images/hero/bg/hero_bg.jpg";
	}

	/**
	 * Site Path
	 *
	 * @return void
	 * @author 
	 **/
	public function sitePath()
	{
		return "ok!";
		return JURI::root();
	}

	/**
	 * Data dump
	 *
	 * @return void
	 * @author 
	 **/
	public function dump($data)
	{

		return var_dump($data);
	}
}