module.exports = function( grunt ) {

  require('load-grunt-tasks')(grunt);
  //uglify  
  //jshint

  grunt.initConfig({
    // srcPath: 'src',
    // buildPath: 'build',
    // pkg: grunt.file.readJSON('package.json'),

    sass: {
      dist: {
        options: {
          style: 'expanded',
          lineNumbers: true,
          sourcemap: 'none'
        },
        files: [{
          expand: true,
          src: ['./src/**/*.scss', './src/**/*.sass', '!_*'],
          dest: './tmp/grunt/sass',
          ext: '.css'
        }]
      }
    },

    cssmin: {
      dist: {
        files: [{
          expand: true,
          cwd: './tmp/grunt/sass',
          src: [ '**/*.css', '!*.min.css' ],
          dest: './tmp/grunt/cssmin',
          ext: '.min.css'
        }]
      }
    },

    concat: {
      options: {
        separator: "\n",
      },
      css: {
        src: [
          './node_modules/bootstrap/dist/css/bootstrap.min.css',
          './node_modules/tether/dist/css/tether.min.css',
          './node_modules/flexslider/flexslider.css',               
          './vendor/SimpleDropDownEffects/css/common.css',
          './tmp/grunt/cssmin/src/interludic2.min.css'
        ],
        dest: './build/app.css'
      },
      js: {
        src: [
          // './vendor/codrops/ClickEffects/js/modernizr.custom.js',
          // './vendor/SimpleDropDownEffects/js/modernizr.custom.63321.js',          
          './vendor/js/modernizr-custom.js',
          './node_modules/jquery/dist/jquery.min.js',
          './vendor/SimpleDropDownEffects/js/jquery.dropdown.js',
          './node_modules/flexslider/jquery.flexslider-min.js',          
          './node_modules/flexslider/demo/js/jquery.fitvid.js',          
          './node_modules/flexslider/demo/js/froogaloop.js',          
          './node_modules/tether/dist/js/tether.min.js',
          './node_modules/bootstrap/dist/js/bootstrap.min.js',
          './vendor/codrops/ClickEffects/js/classie.js',
          './vendor/js/jquery.backstretch.min.js',
          './src/js/**/*.js'
        ],
        dest: './build/app.js'
      }
    },

    copy: {      
      index: {
        files:[
          {expand: true, cwd: 'src/', src: ['*.html'], dest: 'build/'},
          // Copy for gantry
          {expand: true, cwd: 'build/', src: ['app.*'], dest: '../'},
        ]
        // src: ['./src/index.html'],
        // dest: './build/index.html'
      },
      images: {
        files: [
          {
            expand: true,
            cwd: 'src/img/',
            src: ['**/*.{png,jpg,svg}'],
            dest:'build/img/'
          },
          // Copy for gantry          
          {
            expand: true,
            cwd: 'src/img/',
            src: ['**/*.{png,jpg,svg}'],
            dest:'../images/'
          },
        ]
      },
      fonts: {
        files: [
          {
            expand: true,
            cwd: 'src/fonts/',
            src: ['**/*.{ttf,woff,svg,eot}'],
            dest:'build/fonts/'
          }
        ]
      }
    },

    watch: {
      css: {
        files: ['./src/**/*.scss', './src/**/*.sass', './vendor/**/*.scss', './vendor/**/*.css'],
        tasks: [ 'sass', 'newer:cssmin', 'concat:css' ]
      },
      js: {
        files: ['./src/**/*.js', './vendor/**/*.js'],
        tasks: ['concat:js' ]
      },
      html: {
        files: ['./src/*.html', './src/img/**/*'],
        tasks: ['copy']
      }
    }
  });

  grunt.registerTask('default', ['sass', 'cssmin', 'concat', 'copy']);

};
